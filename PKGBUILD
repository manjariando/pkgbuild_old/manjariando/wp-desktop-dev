# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: ValHue <vhuelamo at gmail dot com>
#

_appname=wordpress.com
pkgname=wp-desktop-dev
_pkgver=6.14.0-beta1
pkgver=${_pkgver/-/}
pkgrel=1.1
pkgdesc="WordPress.com Desktop client"
url="https://desktop.wordpress.com"
arch=('x86_64')
license=('GPL2')
makedepends=('imagemagick')
conflicts=('wp-desktop')
provides=("wp-desktop=${pkgver}")

source=("https://github.com/Automattic/wp-calypso/releases/download/v${_pkgver}/${_appname}-linux-x64-${_pkgver}.tar.gz"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://raw.githubusercontent.com/Automattic/wp-desktop/develop/LICENSE.md")
sha256sums=('a7b03833c2cbb8f19d47289846fe379a76f18748ac8ac7cc8fc3541d1e9634ec'
            '4047d3c1aac68a9bc4987b33fcb3e400dc0d2687a1eadf60e9f0e53d5051cab1'
            '85fcf2d203ed1a6471520774fae309bee22b1dafc938985c5b6651d2f23a9a82')

_wpcom_desktop="[Desktop Entry]
Name=WordPress.com
Comment=WordPress.com Desktop Client
Comment[pt_BR]=Cliente Desktop WordPress.com
Exec=wpcom
Icon=wpcom-dev
Type=Application
StartupNotify=true
Categories=Development;Network;"

build() {
    cd "${srcdir}"
    echo -e "$_wpcom_desktop" | tee com.wpcom.desktop
}

package() {
    depends=('alsa-lib' 'gcc-libs' 'gconf' 'gtk2' 'libgpg-error' 'libxss' 'libxkbfile' 'libxtst' 'nss')

    cd "${srcdir}/${_appname}-linux-x64-${_pkgver}"
    install -d ${pkgdir}/usr/share/wpcom
    mv * ${pkgdir}/usr/share/wpcom
    install -d ${pkgdir}/usr/bin

    cd "${srcdir}"
    install -Dm 644 com.wpcom.desktop "${pkgdir}/usr/share/applications/com.wpcom_dev.desktop"
    install -Dm 644 LICENSE.md "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    install -Dm 644 "com.${pkgname}.metainfo.xml" "$pkgdir/usr/share/metainfo/com.${pkgname}.metainfo.xml"

    # Fix and install desktop icons
    for size in 22 24 32 48 64 128 256 512; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${pkgdir}/usr/share/wpcom/resources/app/public_desktop/windows-tray-bubble.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/wpcom-dev.png"
    done

    cd "${pkgdir}/usr/share/wpcom"
    install -Dm644 ./resources/app/public_desktop/app-logo.png ${pkgdir}/usr/share/pixmaps/wpcom-dev.png
    ln -s /usr/share/wpcom/wpcom ${pkgdir}/usr/bin/wpcom

    # Archify permissions
    chmod 4755 "${pkgdir}/usr/share/wpcom/chrome-sandbox"
}
